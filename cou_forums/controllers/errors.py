import random

_LYRICS = [
    "'Cause your friends don't dance and if they don't dance",
    "A place that they will never find",
    "And I can act like an imbecile",
    "And leave the real one far behind",
    "And say",
    "And surprise them with a victory cry",
    "And we can act like we come from out of this world",
    "And we can dance, dansez",
    "And we can dress real neat from our hats to our feet",
    "And you can act real rude and be totally removed",
    "As long as we abuse it, never going to lose it",
    "Everybody look at your hands",
    "Everybody's taking the chance",
    "Everything will work out right",
    "Everything's out of control",
    "I say, we can dance if we want to",
    "I say, we can dance, we can dance",
    "If we don't nobody will",
    "It's a safety dance",
    "Oh it's a safety dance",
    "Oh well it's safe to dance",
    "Say, we can act if we want to",
    "Say, we can go where we want to",
    "The night is young and so am I",
    "We can dance if we want to",
    "We can dance, we can dance",
    "We can go when we want to",
    "We can leave your friends behind",
    "We're doing it from pole to pole",
    "We've got all your life and mine",
    "Well it's a safety dance",
    "Well it's safe to dance",
    "Well they're no friends of mine",
    "Yes it's safe to dance",
]


def lyric() -> str:
    return random.choice(_LYRICS)
