import json

from flask import Blueprint, request, session
from sqlalchemy.orm.exc import NoResultFound

from cou_forums.controllers import require_auth, require_reply_id, respond
from cou_forums.data import db_connect
from cou_forums.data.reply import Reply

voting = Blueprint("voting", __name__, url_prefix="/api/vote")


@voting.route("/up", methods=["POST"])
@require_auth
@require_reply_id
def vote_up():
    reply_id = json.loads(request.data).get("reply_id")
    db = db_connect()

    try:
        reply = db.query(Reply).filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        db.close()
        return respond(ok=False, data="no such reply")

    if reply.user_id == session["user_id"]:
        db.close()
        return respond(ok=False, data="cannot upvote own reply")

    reply.vote_up(session["user_id"])
    db.commit()
    db.close()
    return respond()


@voting.route("/down", methods=["POST"])
@require_auth
@require_reply_id
def vote_down():
    reply_id = json.loads(request.data).get("reply_id")
    db = db_connect()

    try:
        reply = db.query(Reply).filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        db.close()
        return respond(ok=False, data="no such reply")

    if reply.user_id == session["user_id"]:
        db.close()
        return respond(ok=False, data="cannot downvote own reply")

    reply.vote_down(session["user_id"])
    db.commit()
    db.close()
    return respond()


@voting.route("/clear", methods=["POST"])
@require_auth
@require_reply_id
def vote_none():
    reply_id = json.loads(request.data).get("reply_id")
    db = db_connect()

    try:
        reply = db.query(Reply).filter(Reply.entry_id == reply_id).one()
    except NoResultFound:
        db.close()
        return respond(ok=False, data="no such reply")

    reply.clear_vote(session["user_id"])
    db.commit()
    db.close()
    return respond()
