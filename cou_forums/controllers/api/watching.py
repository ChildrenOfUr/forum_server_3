from os import getpid
import json
from threading import Thread

from flask import Blueprint, request, session
from flask_mail import Message
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound

import config
from cou_forums import app, mail
from cou_forums.controllers import require_auth, require_post_id, respond
from cou_forums.data import db_connect
from cou_forums.data.post import Post
from cou_forums.data.user import User


watching = Blueprint("watching", __name__, url_prefix="/api/subscriptions")


@watching.route("/watch", methods=["POST"])
@require_auth
@require_post_id
def add_watcher():
    post_id = json.loads(request.data).get("post_id")
    db = db_connect()

    try:
        post = db.query(Post).filter(Post.entry_id == post_id).one()
    except NoResultFound:
        db.close()
        return respond(ok=False, data="no such post"), 400

    post.add_watcher(session["user_id"])
    db.commit()
    db.close()
    return respond()


@watching.route("/unwatch", methods=["POST"])
@require_auth
@require_post_id
def remove_watcher():
    post_id = json.loads(request.data).get("post_id")
    db = db_connect()

    try:
        post = db.query(Post).filter(Post.entry_id == post_id).one()
    except NoResultFound:
        db.close()
        return respond(ok=False, data="no such post"), 400

    post.remove_watcher(session["user_id"])
    db.commit()
    db.close()
    return respond()


def notify_watchers(reply):
    """Email all users subscribed to a post with the new reply."""

    if len(reply.post.watchers) == 0:
        return

    # This function will run in a separate thread so that the updated thread
    # page will be able to load before all of the emails are sent.
    def _send_emails():
        subject = f'New post reply to "{reply.post.title}"'

        common_html = (
            "<p>You're receiving this message because you subscribed to a post titled "
            f'"{reply.post.title}" and '
            f'<a href="{reply.user.profile_url}" target="_blank">'
            f"{reply.user.username}</a> just replied to it:</p>"
            f"<blockquote>{reply.content}</blockquote>"
            f'<p>To open the reply, click here: <a href="{config.app_uri}{reply.url}">'
            f'{config.app_uri}{reply.url}</a>.</p>'
            "<p>To unsubscribe from future notifications, log in and "
            "click the bell icon next to the post.</p>"
            "<p>Sincerely,<br>"
            f"Bureaucrat #{getpid()}.</p>"
        )

        with app.app_context():
            # Find watchers who are not the author of the new reply
            db = db_connect()
            users = (
                db.query(User)
                .filter(
                    and_(
                        User.user_id != reply.user_id,
                        User.user_id.in_(reply.post.watchers)
                    )
                )
                .all()
            )
            db.close()

            # Write a message to each of them
            messages = [
                Message(
                    sender=("Children of Ur Forums", "forums@childrenofur.com"),
                    recipients=[user.email],
                    subject=subject,
                    html=f"<p>Hi, {user.username}!</p>" + common_html,
                )
                for user in users
            ]

            # Connect to the server and deliver all messages
            with mail.connect() as smtp:
                for msg in messages:
                    smtp.send(msg)

    Thread(target=_send_emails).start()
