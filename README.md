# Forum Server v3

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)
[![pipeline status](https://gitlab.com/ChildrenOfUr/forum_server_3/badges/master/pipeline.svg)](https://gitlab.com/ChildrenOfUr/forum_server_3/commits/master)

A custom forums server written in Python
using Flask to serve dynamic pages
and SQLAlchemy to interact with our userdata database.

## Contributing

Merge requests with bug fixes and new features are welcome!

### Project structure

- `controllers` contains the code that handles different URLs,
  such as `/user/...` or `/api/...` as well as definitions 
  and functions used by multiple pages.
  - `controllers/util.py` contains helpful annotations for
  endpoint functions, such as requiring authentication or parameters.
- `data` contains tools for interacting with the database and
  the classes that represent database objects.
- `static` contains the web resources linked from HTML documents.
- `templates` contains the HTML files Flask completes and serves.
    - `templates/common` contains some HTML fragments that these
      templates can include, such as the navbar and post preview.
- `app.py` is the main file for the application that starts
  the server and contains server configuration functions.

### Setting up a development environment

We suggest [JetBrains PyCharm](https://www.jetbrains.com/pycharm/)
as it will handle all of the following tasks automatically.

1. You'll need Python 3.7 available to run the server, as well as pipenv:

   `python3 -m pip install pipenv`
    
2. Install the server and its dependencies from the clone directory:

   `pipenv install --dev`
   
3. Set these environment variables:    

    - SECRET_KEY - used to store cookies and can be any string for local use
    - DB_URI - full, authenticated URI to a PostgreSQL database
    - GAME_SERVER_URI - protocol (http or https) and hostname of the game/auth servers
    - MAIL_SERVER - the hostname of an SMTP server
    - MAIL_USERNAME - the username to log in to the SMTP server
    - MAIL_PASSWORD - the password for MAIL_USERNAME@MAIL_SERVER
    - PORT - (optional, defaults to 8583) the port to bind to (and this may be overridden by your run configuration anyway)
    - APP_URI - the host running the application (most likely http://localhost:$PORT)
    
    In PyCharm, this is set in your run configuration.
   
4. Run the server locally:

   `PYTHONPATH=. python3 app.py`
   
5. Navigate to [localhost:8583](http://localhost:8583) to view the running application.
