import json

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from cou_forums.data import Base
from cou_forums.data.entry import Entry


class Reply(Base, Entry):
    __tablename__ = "replies"

    _upvoters = Column("upvoters", String, nullable=False, default="[]")
    _downvoters = Column("downvoters", String, nullable=False, default="[]")

    def __init__(self, content, post_id, user_id):
        self.user_id = user_id
        self.post_id = post_id
        self.content = content

    @declared_attr
    def post_id(self):
        return Column(Integer, ForeignKey("posts.id"))

    @declared_attr
    def post(self):
        return relationship("Post", lazy="joined")

    @property
    def upvoters(self) -> set:
        return set(json.loads(self._upvoters))

    @property
    def downvoters(self) -> set:
        return set(json.loads(self._downvoters))

    def vote_up(self, user_id: int):
        self.clear_vote(user_id)
        self._upvoters = json.dumps(list(self.upvoters | {user_id}))

    def vote_down(self, user_id: int):
        self.clear_vote(user_id)
        self._downvoters = json.dumps(list(self.downvoters | {user_id}))

    def clear_vote(self, user_id: int):
        if user_id in self.upvoters:
            self._upvoters = json.dumps(list(self.upvoters - {user_id}))

        if user_id in self.downvoters:
            self._downvoters = json.dumps(list(self.downvoters - {user_id}))

    @property
    def score(self) -> int:
        return len(self.upvoters) - len(self.downvoters)

    @property
    def url(self) -> str:
        return f"{self.post.url}#reply-{self.entry_id}"

    @property
    def edit_url(self):
        return f"/reply/edit/{self.entry_id}"
