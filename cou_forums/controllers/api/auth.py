import json

from flask import Blueprint, request, session

import config
from cou_forums.controllers import respond
from cou_forums.data import db_connect
from cou_forums.data.user import User

AUTH_SERVER = f"{config.game_server_uri}:8383/auth"
auth = Blueprint("auth", __name__, url_prefix="/api/auth")


@auth.route("/login", methods=["POST"])
def login_api():
    if not request.data:
        return respond(False, "missing login data"), 400

    db = db_connect()
    session["auth"] = json.loads(request.data)
    session["user_id"] = (
        db.query(User).filter(User.email == session["auth"]["playerEmail"]).one()
    ).user_id
    db.close()
    return respond()
