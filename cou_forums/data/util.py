from datetime import datetime


def plural_s(quantity, singular=str(), plural="s") -> str:
    return singular if quantity == 1 else plural


def rel_time(dt: datetime) -> str:
    age = datetime.utcnow() - dt

    if age.days >= 1:
        years = age.days // 365
        if years >= 1:
            return f"{years} year{plural_s(years)} ago"

        months = age.days // 30
        if months >= 1:
            return f"{months} month{plural_s(months)} ago"

        weeks = age.days // 7
        if weeks >= 1:
            return f"{weeks} week{plural_s(weeks)} ago"

        return f"{age.days} day{plural_s(age.days)} ago"

    hours = age.seconds // 3600
    if hours >= 1:
        return f"{hours} hour{plural_s(hours)} ago"

    minutes = age.seconds // 60
    if minutes >= 1:
        return f"{minutes} minute{plural_s(minutes)} ago"

    if age.seconds > 0:
        return f"{age.seconds} second{plural_s(age.seconds)} ago"

    return "just now"
