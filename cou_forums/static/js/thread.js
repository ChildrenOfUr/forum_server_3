$(function () {
    $("[data-vote]").on("click", function () {
        let button = $(this);
        $(button).button("loading");
        let direction = $(button).data("vote");
        let reply = $(button).closest(".reply");
        let score = $('.reply-score[data-reply-id="' + $(reply).data("reply-id") + '"]');
        let delta = {
            "up": 1,
            "down": -1
        }[direction];

        // If we are undoing a vote, the score will
        // move the opposite direction.
        if ($(button).hasClass("active")) {
            direction = "clear";
            delta *= -1;
        }

        // If a different button is active,
        // undoing the other vote and applying this one will
        // result in a total change of 2 points.
        if ($(reply).find(".active[data-vote]").not(button).length) {
            delta *= 2;
        }

        $.ajax({
            type: "POST",
            url: "/api/vote/" + direction,
            data: JSON.stringify({
                "reply_id": $(reply).data("reply-id")
            }),
            contentType: "application/json",
            dataType: "json",
            success: function () {
                // Update the score
                $(score).text(parseInt($(score).text()) + delta);

                // Mark all buttons as inactive
                $(reply).find("[data-vote]").removeClass("active");

                // Mark the new button as active
                if (direction !== "clear") {
                    $(button).addClass("active");
                }
            },
            complete: function () {
                $(button).button("reset");
            }
        });
    });

    $("[data-delete-reply-id]").on("click", function () {
        if (!confirm("Are you sure you want to delete this reply?")) {
            return;
        }

        let replyId = $(this).data("delete-reply-id");
        let reply = $('[data-reply-id="' + replyId + '"]');
        $(this).prop("disabled", true);

        $.ajax({
            type: "POST",
            url: "/api/delete/reply",
            data: JSON.stringify({
                "reply_id": replyId
            }),
            contentType: "application/json",
            dataType: "json",
            complete: function (jqxhr, status) {
                if (status === "success") {
                    reply.remove();
                } else {
                    $(this).prop("disabled", false);
                    alert("Could not delete reply. Please try again later.\n\n"
                        + "Error: " + $.parseJSON(jqxhr.responseText)["data"]);
                }
            }
        });
    });
});

function deletePost(btn) {
    if (!confirm("Are you sure you want to delete this post? This will also delete all replies to it!")) {
        return;
    }

    $(btn).prop("disabled", true);
    let postId = $(btn).data("delete-post-id");

    $.ajax({
        type: "POST",
        url: "/api/delete/post",
        data: JSON.stringify({
            "post_id": postId
        }),
        contentType: "application/json",
        dataType: "json",
        complete: function (jqxhr, status) {
            if (status === "success") {
                alert("Post deleted successfully!");
                window.location.href = $.parseJSON(jqxhr.responseText)["data"];
            } else {
                $(btn).prop("disabled", false);
                alert("Could not delete reply. Please try again later.\n\n"
                    + "Error: " + $.parseJSON(jqxhr.responseText)["data"]);
            }
        }
    });
}

function toggleSubscription(btn) {
    let action = {
        true: "unwatch",
        false: "watch"
    }[$(btn).hasClass("active")];

    $(btn).button("loading");

    $.ajax({
        type: "POST",
        url: "/api/subscriptions/" + action,
        data: JSON.stringify({
            "post_id": $(btn).data("watch-post-id")
        }),
        contentType: "application/json",
        dataType: "json",
        success: function () {
            $(btn).toggleClass("active");
        },
        error: function (jqxhr) {
            alert("Could not " + action + " post.\n\n"
                + $.parseJSON(jqxhr.responseText)["data"]);
        },
        complete: function () {
            $(btn).button("reset");
        }
    });
}
