$(function () {
    tinymce.init({
        selector: "textarea",
        min_height: 100,
        height: 300,
        content_css: "/static/css/tinymce.css",
        plugins: [
            "code",
            "emoticons",
            "image",
            "link",
            "lists",
            "paste",
            "preview",
            "table"
        ],
        image_caption: true,
        menubar: false,
        toolbar: "undo redo |"
            + "cut copy paste pastetext | "
            + "styleselect forecolor removeformat | "
            + "bullist numlist | "
            + "link unlink | "
            + "image table hr emoticons | "
            + "code preview",
        toolbar_drawer: "sliding",
        contextmenu: false
    });
});

function submitEntry(type, callback, id = null, postId = null, title = null, category = null) {
    if (type !== "post" && type !== "reply") {
        return false;
    }

    let content = tinyMCE.activeEditor.getContent().trim();

    if (type === "reply" && !content) {
        alert("You can't submit an empty " + type + ".");
        callback(null, false);
        return;
    }

    let data = {
        "content": content
    };

    if (id) {
        // Editing a post or reply
        data[type + "_id"] = id;
    } else if (type === "reply" && postId) {
        // New reply to post
        data["post_id"] = postId;
    } else {
        // New post
        data["category"] = category;
    }

    if (type === "post") {
        if (!(title = title.trim())) {
            alert("Post titles can't be empty.");
            callback(null, false);
            return;
        }

        data["title"] = title;
    }

    $.ajax({
        type: "POST",
        url: "/api/submit/" + type,
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        complete: function (jqxhr, status) {
            if (status !== "success") {
                alert("Could not save " + type + ". Please try again later.\n\n"
                    + "Error: " + $.parseJSON(jqxhr.responseText)["data"]);
            }

            callback(jqxhr, status);
        }
    });
}

function submitNewReply(btn) {
    let postId = $("[data-post-id]").data("post-id");
    $(btn).button("loading");

    submitEntry("reply", function (jqxhr, status) {
        if (status === "success") {
            tinyMCE.activeEditor.setContent("");
            window.location.reload();
        } else {
            $(btn).button("reset");
        }
    }, null, postId);
}

function submitStandalone(btn) {
    $(btn).button("loading", true);
    let entryId = $(btn).data("entry-id");
    let entryType = $(btn).data("entry-type");
    let title = null;
    let category = null;

    if (entryType === "post") {
        title = $("#entry-title").val();
        category = $("#entry-category").val();
    }

    submitEntry(entryType, function (jqxhr, status) {
        if (status === "success") {
            tinyMCE.activeEditor.setContent("");
            window.location.href = $.parseJSON(jqxhr.responseText)["data"];
        } else {
            $(btn).button("reset");
        }
    }, entryId, null, title, category);
}
